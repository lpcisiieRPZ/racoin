<?php
/**
 * Created by PhpStorm.
 * User: locoman
 * Date: 12/01/16
 * Time: 15:47
 */

namespace racoin\backend\controller;

use model\User;
use model\Annonce;

class indexController
{
    public static function index($app, $action = null, $id = null)
    {
        if(isset($action)) {
            switch ($action) {
                case 'index' :
                    self::index2($app);
                    break;
                case 'deconnection' :
                    self::deconnection($app);
                    break;
                case 'connection' :
                    self::connection($app);
                    break;
                case 'admin' :
                    self::admin($app);
                    break;
                case 'annonceId' :
                    self::annonceId($app, $id);
                    break;
                case 'delete' :
                    self::delete($app, $id);
                    break;
                case 'accept' :
                    self::accept($app, $id);
                    break;
                default :
                    echo "Insane in the membrane";
                    break;
            }
        }else{
            self::index($app);
        }
    }

    public static function index2($app){
        if (isset($_SESSION['privilege'])) {
            if (session_status() === PHP_SESSION_ACTIVE && $_SESSION['privilege'] === "admin") {
                $app->redirect($app->urlFor('indexAction', array("action" => "admin")));
            } else {
                $app->render('index.html.twig');
            }
        } else
            $app->render('index.html.twig');
    }

    public static function deconnection($app)
    {
        session_destroy();
        unset($_SESSION);
        $app->render('index.html.twig');
    }

    public static function connection($app)
    {
        if ($app->request->headers->get('Content-Type', 'application/x-www-form-urlencoded')) {
            $email = filter_var($app->request->post('email'), FILTER_SANITIZE_STRING);
            $password = filter_var($app->request->post('password'), FILTER_SANITIZE_STRING);

            if (isset($email) && isset($password)) {
                if (User::where("email", '=', $email)->first()) {
                    $user_db = User::where("email", '=', $email)->first();

                    if (password_verify($password, $user_db->password)) {
//                        session_start();
                        $_SESSION['privilege'] = "admin";

                        $app->response->setStatus(200);
                        $app->redirect($app->urlFor('indexAction', array("action" => "admin")));
//                        header("Location : /admin");
//                        indexController::admin($app);
                    } else
                        echo "wrong credential";
                } else
                    echo "erreur email";
            }
        }else{
            echo "wrong form";
        }
    }

    public static function admin($app)
    {
        if (isset($_SESSION['privilege'])) {
            if (session_status() === PHP_SESSION_ACTIVE && $_SESSION['privilege'] === "admin") {
                $annonces = Annonce::select('titre', 'created_at', 'prix', 'id', 'status', 'id')->where('status', '=', '1')->orderBy('created_at', 'ASC')->take(5)->get();

                $app->render('admin.html.twig', array(
                    'annonces' => $annonces
                ));
            } else
                $app->redirect($app->urlFor('index'));
        } else
            $app->redirect($app->urlFor('index'));
    }

    public static function annonceId($app, $id)
    {
        $id_annonce = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $annonce = Annonce::find($id_annonce);
        $annonce_infos['id'] = $annonce->id;
        $annonce_infos['titre'] = $annonce->titre;
        $annonce_infos['descriptif'] = $annonce->descriptif;
        $annonce_infos['ville'] = $annonce->ville;
        $annonce_infos['code_postal'] = $annonce->code_postal;
        $annonce_infos['prix'] = $annonce->prix;
        $annonce_infos['nom_a'] = $annonce->nom_a;
        $annonce_infos['prenom_a'] = $annonce->prenom_a;
        $annonce_infos['mail_a'] = $annonce->mail_a;
        $annonce_infos['tel_a'] = $annonce->tel_a;
        $annonce_infos['cat_id'] = $annonce->cat_id;
        $annonce_infos['dep_id'] = $annonce->dep_id;
        $annonce_infos['created_at'] = $annonce->created_at;
        $annonce_infos['updated_at'] = $annonce->updated_at;

        $app->render('annonce.html.twig', array(
            'annonce' => $annonce_infos
        ));
    }

    public static function delete($app, $id)
    {
        if(($annonce = Annonce::find($id)) != null ){
            $annonce->status = 0;
            $annonce->save();

            $app->redirect($app->urlFor('indexAction', array("action" => "admin")));

        }else{
            $app->render('admin.html.twig', array(
                'annonce' => $annonce,
                'truc' => 'fail'
            ));
//            echo "fail";
        }
    }

    public static function accept($app, $id)
    {
        if(($annonce = Annonce::find($id)) != null ){
            $annonce->status = 2;
            $annonce->save();

            $app->redirect($app->urlFor('indexAction', array("action" => "admin")));

        }else{
            $app->render('admin.html.twig', array(
                'annonce' => $annonce,
                'truc' => 'fail'
            ));
//            echo "fail";
        }
    }
}