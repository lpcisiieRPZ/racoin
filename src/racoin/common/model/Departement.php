<?php
/**
 * Created by PhpStorm.
 * User: locoman
 * Date: 08/12/15
 * Time: 16:52
 */

namespace model;


class Departement extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'departement';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function dep_annonce (){
        return $this->hasMany('model\Annonce', 'dep_id');
    }
}