<?php

/**
 * Created by PhpStorm.
 * User: locoman
 * Date: 08/12/15
 * Time: 16:43
 */
namespace model;

class Annonce extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'annonce';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function categorie(){
        return $this->belongsTo('model\Categorie', 'cat_id');
    }

    public function departement(){
        return $this->belongsTo('model\Departement', 'dep_id');
    }
}