<?php
/**
 * Created by PhpStorm.
 * User: locoman
 * Date: 13/01/16
 * Time: 00:45
 */

namespace model;


class User extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;
}