<?php
/**
 * Created by PhpStorm.
 * User: locoman
 * Date: 08/12/15
 * Time: 16:51
 */

namespace model;

class Categorie extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'categorie';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function cat_annonce (){
        return $this->hasMany('model\Annonce', 'cat_id')->select('titre', 'prix', 'id')->orderBy('created_at', 'DESC');
    }
}