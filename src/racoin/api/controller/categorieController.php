<?php

namespace racoin\backend\controller;

use model\Annonce;
use model\Categorie;

class categorieController
{
    public static function categories($app)
    {
        $app->response->headers->set('Content-Type', 'application/json');

        if(Categorie::all()){
            $app->response->headers->set('Content-Type', 'application/json');
            $categories = Categorie::select('id','libelle')->orderBy('libelle')->get();
            if(count($categories) !=0){
                for($i=0; $i < count($categories); $i++){
                    $categories[$i]->links = array(
                        "categ" => $app->urlFor('categoriesId', ['id' => $categories[$i]->id])
                    );
                }
                echo json_encode($categories);
                $app->response->setStatus(201);
            }
            else{
                $app->response->setStatus(500);
                echo json_encode(array(
                    "Error" => 400,
                    "Object" => "categorie",
                    "Message" => "Il n'y a pas de categorie"
                ));
            }
        }
        else{
            $app->response->setStatus(500);
            echo json_encode(array(
                "Error" => 400,
                "Object" => "categorie",
                "Message" => "Cette categorie n'exsite pas"
            ));
        }
    }

    public static function categorieId($app, $id)
    {
        if(filter_var($id, FILTER_SANITIZE_NUMBER_INT)) {
            $app->response->headers->set('Content-Type', 'application/json');
            if (Categorie::find($id)) {
                $app->response->headers->set('Content-Type', 'application/json');

                $categories = Categorie::find($id);

                $obj = array(
                    "categorie" => $categories,
                    "links" => array(
                        "Annonces" => array(
                            "href" => $app->urlFor('categoriesAnnonce', ['id' => $categories->id])
                        )
                    )
                );

                echo json_encode($obj);
                $app->response->setStatus(201);
            } else {
                $app->response->setStatus(400);
                echo json_encode(array(
                    "Error" => 400,
                    "Object" => "categ_id",
                    "Message" => "cet ID de categorie n'existe pas"
                ));
            }
        }
    }
}