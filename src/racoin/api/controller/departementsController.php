<?php
/**
 * Created by PhpStorm.
 * User: locoman
 * Date: 10/12/15
 * Time: 16:31
 */

namespace racoin\backend\controller;

use model\Departement;

class departementsController
{
    public static function departementsId($app, $id){
        if(filter_var($id, FILTER_SANITIZE_NUMBER_INT)) {
            $app->response->headers->set('Content-Type', 'application/json');
            $app->response->setStatus(400);
            if (Departement::find($id)) {
                $app->response->headers->set('Content-Type', 'application/json');
                echo Departement::find($id);
                $app->response->setStatus(201);
            } else {
                $app->response->setStatus(400);
                echo json_encode(array(
                    "Error" => 400,
                    "Object" => "dept_id",
                    "Message" => "Cet ID de département n'existe pas"
                ));
            }
        }
    }
}