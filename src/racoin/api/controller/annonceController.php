<?php

namespace racoin\backend\controller;

use Illuminate\Support\Facades\DB;
use model\Annonce;
use model\Categorie;
use model\Departement;

class annonceController
{
	public static function annonces($app, $id = -1)
	{
		$app->response->headers->set('Content-Type', 'application/json');
		$data = $app->request->get();
		if (Annonce::all()) {
			if ($id != -1) {
				if (Annonce::find($id))
					$annonces = Annonce::select('titre', 'prix', 'id')->where('cat_id', '=', filter_var($id, FILTER_SANITIZE_NUMBER_INT))->where('status', '=', '2');
				else {
					$app->response->setStatus(400);
					echo json_encode(array(
							"Error" => 400,
							"Object" => "cat_id",
							"Message" => "L'id de la catégorie n'existe pas"
					));
				}

			} else {
				$annonces = Annonce::select('titre', 'prix', 'id')->where('status', '=', '2')->orderBy('created_at', 'DESC');
			}

			if (isset($data['min'])) {
				$annonces->where('prix', '>=', filter_var($data['min'], FILTER_SANITIZE_NUMBER_INT));
			}
			if (isset($data['max'])) {
				$annonces->where('prix', '<=', filter_var($data['max'], FILTER_SANITIZE_NUMBER_INT));
			}
			if (isset($data['take'])) {
				$annonces->take(filter_var($data['take'], FILTER_SANITIZE_NUMBER_INT));
			}
			if (isset($data['skip'])) {
				$annonces->skip(filter_var($data['skip'], FILTER_SANITIZE_NUMBER_INT));
			}

			// finally send and get the result of the request
			$result = $annonces->get();

			if (count($result) != 0) {
				for ($i = 0; $i < count($result); $i++) {
					$result[$i]->links = array(
							"self" => array(
									"href" => $app->urlFor('annoncesId', ['id' => $result[$i]->id])
							)
					);
				}

				$obj = array(
						"annonces" => $result,
						"links" => ""
				);

				echo json_encode($obj);
			} else {
				$app->response->setStatus(418);
				echo json_encode(array(
						"Response" => 418,
						"Object" => "status",
						"Message" => "Il n'y a pas d'annonce correspondant à la recherche"
				));
			}
		} else {
			$app->response->setStatus(500);
			echo json_encode(array(
					"Error" => 500,
					"Object" => "unknown",
					"Message" => "Une erreur s'est produite"
			));
		}
	}


	// retourne une annonce par l'id

	public static function catAnnonces($app, $id)
	{
		if(filter_var($id, FILTER_SANITIZE_NUMBER_INT)) {
			$app->response->headers->set('Content-Type', 'application/json');
			if (Annonce::find($id)) {
				$app->response->headers->set('Content-Type', 'application/json');
				echo Annonce::find($id)->categorie->tojson();
				$app->response->setStatus(201);
			} else {
				$app->response->setStatus(400);
				echo json_encode(array(
					"Error" => 400,
					"Object" => "id",
					"Message" => "Cet ID d'annonce n'existe pas"
				));
			}
		}
	}

	// Add a new annonce
	public static function addAnnonce($app)
	{
		// set headers for response
		$app->response->headers->set('Content-Type', 'application/json');
		$cat_id = null;
		$url = explode("/", $app->request()->getPath());

		// if the request use json
		if ($app->request->headers->get('Content-Type') == 'application/json') {
			// get the json en parses it
			$data = json_decode($app->request->getBody(), true);
			// if request is a html-form
		} elseif ($app->request->headers->get('Content-Type') == 'application/x-www-form-urlencoded') {
			$data = $app->request->post();
		} else {
			$app->response->setStatus(400);
			echo json_encode(array(
					"Error" => 400,
					"Object" => "unknown",
					"Message" => "Mauvaise déclaration du content-type"
			));
		}

		// If the request come from /categories/ route
		if($url[3] == "categories"){
			$cat_id = filter_var($url[4], FILTER_SANITIZE_NUMBER_INT);
			$date['cat_id'] = $cat_id;
		}else{
			if(isset($data['cat_id']))
				$cat_id = filter_var($data['cat_id'], FILTER_SANITIZE_NUMBER_INT);
		}

		// throw exception if any important variable is missing
		if (isset($data['titre'], $data['descriptif'], $data['ville'], $data['code_postal'], $data['prix'], $data['dep_id'], $data['password'], $cat_id) ) {
			// test if the category exists
			if (Categorie::find($cat_id)) {
				// Test if the department exists
				if (Departement::find(filter_var($data['dep_id'], FILTER_SANITIZE_NUMBER_INT))) {

					$hash = password_hash(filter_var($data['password'], FILTER_SANITIZE_STRING), PASSWORD_DEFAULT);

					// creation of new instance of an annonce
					$annonce = new Annonce();

					// set the annonce's parameters
					$annonce->titre = filter_var($data['titre'], FILTER_SANITIZE_STRING);
					$annonce->descriptif = filter_var($data['descriptif'], FILTER_SANITIZE_STRING);
					$annonce->ville = filter_var($data['ville'], FILTER_SANITIZE_STRING);
					$annonce->code_postal = filter_var($data['code_postal'], FILTER_SANITIZE_NUMBER_INT);
					$annonce->prix = filter_var($data['prix'], FILTER_SANITIZE_NUMBER_FLOAT);
					$annonce->passwd = $hash;
					$annonce->cat_id = $cat_id;
					$annonce->dep_id = filter_var($data['dep_id'], FILTER_SANITIZE_NUMBER_INT);
					$annonce->date_online = NULL;
					$annonce->created_at = time();
					$annonce->updated_at = NULL;
					$annonce->status = 1;

					// throw exception if the record failed
					try {
						$annonce->save();

						$rootURI = $app->request->getRootUri();
						$app->response->setStatus(201);
						header("Location : $rootURI/annonces/$annonce->id");
						annonceController::annonceId($app, $annonce->id);
					} catch (\Exception $e) {
						$app->response->setStatus(500);
						echo json_encode(array(
								"Error" => 500,
								"Message" => "l'enregistrement a échoué : $e->getMessage()"
						));
					}
				} else {
					$app->response->setStatus(400);
					echo json_encode(array(
							"Error" => 400,
							"Object" => "dep_id",
							"Message" => "L'id du département n'existe pas"
					));
				}
			} else {
				$app->response->setStatus(400);
				echo json_encode(array(
						"Error" => 400,
						"Object" => "cat_id",
						"Message" => "L'id de la categorie n'existe pas"
				));
			}

		} else {
			$app->response->setStatus(400);
			echo json_encode(array(
					"Error" => 400,
					"Message" => "Il manque des variables pour enregistrer l'annonce."
			));
		}
	}

	// Add a new annonce

	public static function annonceId($app, $id)
	{
		if(filter_var($id, FILTER_SANITIZE_NUMBER_INT)) {
			$app->response->headers->set('Content-Type', 'application/json');
			if (Annonce::find($id)) {
				$app->response->headers->set('Content-Type', 'application/json');
				$status = Annonce::select('status')->where('id', '=', $id)->first();
				$annonce = Annonce::select(['id', 'titre', 'descriptif', 'ville', 'code_postal', 'prix', 'date_online', 'cat_id', 'dep_id'])->where('id', '=', $id)->first();
				if ($status->status == 2) {
					$obj = array(
						"annonce" => $annonce, "links" => array(
							'categorie' => array('href' => $app->urlFor('categAnnonce', ['id' => $annonce->id])),
							"departement" => array('href' => $app->urlFor('depAnnonce', ['id' => $annonce->id])))

					);
					echo json_encode($obj);
				} else {
					$app->response->setStatus(400);
					echo json_encode(array(
						"Error" => 400,
						"Object" => "id",
						"Message" => "Cet ID d'annonce n'existe pas"
					));
				}

			} else {
				$app->response->setStatus(400);
				echo json_encode(array(
					"Error" => 400,
					"Object" => "id",
					"Message" => "Cet ID d'annonce n'existe pas"
				));
			}
		}
	}

	public static function annonceStatus($app, $id)
	{
		if(filter_var($id, FILTER_SANITIZE_NUMBER_INT)) {
			$app->response->headers->set('Content-Type', 'application/json');
			if (Annonce::find($id)) {
				//action pour une modification en json à partir du body
				if ($app->request->headers->get('Content-Type', 'application/json')) {
					$json = $app->request->getBody();
					$data = json_decode($json);
					$modif = Annonce::find($id);
					$modif->status = filter_var($data->status, FILTER_SANITIZE_NUMBER_INT);
					$modif->save();
					$app->response->setStatus(201);
					echo json_encode(array(
						"Response" => 201,
						"Object" => "Status",
						"Message" => "Modification OK"
					));
				} elseif ($app->request->headers->get('Content-Type', 'application/x-www-form-urlencoded')) {
					$modif = Annonce::find($id);
					$modif->status = filter_var($app->request->post('status'), FILTER_SANITIZE_NUMBER_INT);
					$modif->save();
					$app->response->setStatus(201);
					echo json_encode(array(
						"Response" => 201,
						"Object" => "Status",
						"Message" => "Modification OK"
					));
				}
			} else {
				$app->response->setStatus(400);
				echo json_encode(array(
					"Error" => 400,
					"Object" => "unknown",
					"Message" => "Cet ID d'annonce n'existe pas"
				));
			}
		}
	}

	public static function deleteAnnonce($app, $id)
	{
		if(filter_var($id, FILTER_SANITIZE_NUMBER_INT)) {
			$app->response->headers->set('Content-Type', 'application/json');
			if (Annonce::find($id)) {
				if ($app->request->headers->get('Content-Type', 'application/json')) {
					$json = $app->request->getBody();
					$data = json_decode($json);
					$modif = Annonce::find($id);
                    $cleanMdp = filter_var($data->password, FILTER_SANITIZE_STRING);
					if (password_verify($cleanMdp, $modif->passwd)) {
						$modif->status = 0;
						$modif->save();
						$app->response->setStatus(201);
						echo json_encode(array(
							"Response" => 201,
							"Object" => "Delete",
							"Message" => "Annonce delete"
						));
					} else {
						$app->response->setStatus(400);
						echo json_encode(array(
							"Error" => 400,
							"Object" => "password",
							"Message" => "Mauvais mot de passe"
						));
					}
				} elseif ($app->request->headers->get('Content-Type', 'application/x-www-form-urlencoded')) {
					$modif = Annonce::find($id);
                    $cleanMdp = filter_var($app->request->post('password'), FILTER_SANITIZE_STRING);
					if (password_verify($cleanMdp, $modif->passwd)) {
						$modif->status = 0;
						$modif->save();
						$app->response->setStatus(201);
						echo json_encode(array(
							"Response" => 201,
							"Object" => "Delete",
							"Message" => "Annonce delete"
						));
					} else {
						$app->response->setStatus(400);
						echo json_encode(array(
							"Error" => 400,
							"Object" => "unknown",
							"Message" => "Mauvais mot de passe"
						));
					}
				}
			} else {
				$app->response->setStatus(400);
				echo json_encode(array(
					"Error" => 400,
					"Object" => "id",
					"Message" => "Cet ID d'annonce n'existe pas"
				));
			}
		}
	}
}