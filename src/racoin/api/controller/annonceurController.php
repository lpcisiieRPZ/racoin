<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 10/12/15
 * Time: 15:14
 */

namespace racoin\api\controller;

use model\Annonce;


class annonceurController
{
    //affichage de l'annonceur via son id
    //on affiche les informations en même temps que l'annonce
    public static function viewAnnonceur($app, $id)
    {
        if(filter_var($id, FILTER_SANITIZE_NUMBER_INT)) {
            $app->response->headers->set('Content-Type', 'application/json');
            if (Annonce::find($id)) {
                $app->response->headers->set('Content-Type', 'application/json');
                $annonce = Annonce::select(['id', 'titre', 'descriptif', 'ville', 'code_postal', 'prix', 'date_online', 'cat_id', 'dep_id'])->where('id', '=', $id)->first();
                $annonceur = Annonce::select(['nom_a', 'prenom_a', 'mail_a', 'tel_a'])->where('id', '=', $id)->first();
                $obj = array(
                    "annonce" => array(
                        "produit" => $annonce,
                        "annonceur" => $annonceur
                    ),
                    "links" => array(
                        "categorie" => array(
                            "href" => $app->urlFor('categoriesId', ['id' => $annonce->cat_id])
                        ),
                        "departement" => array(
                            "href" => $app->urlFor('departementsId', ['id' => $annonce->dep_id]))
                    )
                );
                echo json_encode($obj);
                $app->response->setStatus(201);
            } else {
                $app->response->setStatus(400);
                echo json_encode(array(
                    "Error" => 400,
                    "Object" => "view_annonce",
                    "Message" => "cet ID d'annonce n'existe pas"
                ));
            }
        }
    }

    //ajout de coordonée à une annonce en fonction de l'id
    public static function addCoord($app, $id)
    {
        if(filter_var($id, FILTER_SANITIZE_NUMBER_INT)) {
            $app->response->headers->set('Content-Type', 'application/json');
            if (Annonce::find($id)) {
                //action pour une modification en json à partir du body
                if ($app->request->headers->get('Content-Type', 'application/json')) {
                    $json = $app->request->getBody();
                    $data = json_decode($json);


                    $modif = Annonce::find($id);
                    $modif->nom_a = filter_var($data->nom, FILTER_SANITIZE_STRING);
                    $modif->prenom_a = filter_var($data->prenom, FILTER_SANITIZE_STRING);
                    $modif->mail_a = filter_var($data->mail, FILTER_SANITIZE_EMAIL);
                    $modif->tel_a = filter_var($data->tel, FILTER_SANITIZE_STRING);
                    $modif->save();
                } elseif ($app->request->headers->get('Content-Type', 'application/x-www-form-urlencoded')) {
                    $modif = Annonce::find($id);
                    $modif->nom_a = filter_var($app->request->post('nom'), FILTER_SANITIZE_STRING);
                    $modif->prenom_a = filter_var($app->request->post('prenom'), FILTER_SANITIZE_STRING);
                    $modif->mail_a = filter_var($app->request->post('mail'), FILTER_SANITIZE_EMAIL);
                    $modif->tel_a = filter_var($app->request->post('tel'), FILTER_SANITIZE_STRING);
                    $modif->save();
                }
                $app->response->setStatus(201);
                echo json_encode(array(
                    "Response" => 201,
                    "Object" => "addcoord",
                    "Message" => "Ajout des coordonnees"
                ));

            } else {
                $app->response->setStatus(400);
                echo json_encode(array(
                    "Error" => 400,
                    "Object" => "addCoord",
                    "Message" => "cet ID d'annonce n'existe pas"
                ));
            }
        }
    }

    public static function viewAnnnonceAnnonceur($app, $mail)
    {
        $app->response->headers->set('Content-Type', 'application/json');

        if (Annonce::where('mail_a', '=', $mail)->first()) {
            if ($mail != -1) {
                $annonces = Annonce::select('titre', 'prix', 'id')->where('mail_a', '=', $mail)->get();
            } else {
                $app->response->setStatus(400);
                echo json_encode(array(
                    "Error" => 400,
                    "Object" => "mail_a",
                    "Message" => "L'adresse mail n'existe pas"
                ));
            }
            if (count($annonces) != 0) {
                for ($i = 0; $i < count($annonces); $i++) {
                    $annonces[$i]->links = array(
                        "self" => array(
                            "href" => $app->urlFor('annoncesId', ['id' => $annonces[$i]->id])
                        )
                    );
                }

                $obj = array(
                    "annonces" => $annonces,
                    "links" => ""
                );

                echo json_encode($obj);
                $app->response->setStatus(201);
            }
        } else {
            $app->response->setStatus(400);
            echo json_encode(array(
                "Response" => 400,
                "Object" => "view_annonce_annonceur",
                "Message" => "Cet annonceur n'a pas d'annonces"
            ));
        }
    }
}