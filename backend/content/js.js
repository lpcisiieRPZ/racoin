/**
 * Created by LocoMan on 21/01/2016.
 */

$(function(){
    var validate = function(id) {
        var req = new XMLHttpRequest();
        req.open('PUT', '/racoin/api/annonces/' + id + '/status?apikey=test1234', true);
        req.onreadystatechange = function (aEvt) {
            if (req.readyState == 4) {
                if(req.status == 200)
                    console.log(req.responseText);
                else
                    console.log("Erreur pendant le chargement de la page.\n");
            }
        };
        req.send(null);
    };

    var decline = function(id) {
        var req = new XMLHttpRequest();
        req.open('DELETE', '/racoin/api/annonces/' + id + "?apikey=test1234", true);
        req.onreadystatechange = function (aEvt) {
            if (req.readyState == 4) {
                if(req.status == 200)
                    console.log(req.responseText);
                else
                    console.log("Erreur pendant le chargement de la page.\n");
            }
        };
        req.send(null);
    };

    $('#validate_annonce').click(function(){
        var id = $('#validate_annonce').attr('name');
        validate(id);
    });

    $('#decline_annonce').click(function(){
        var id = $('#decline_annonce').attr('name');
        decline(id);
    });

});