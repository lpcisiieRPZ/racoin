 <?php
/**
 * Created by PhpStorm.
 * User: locoman
 * Date: 12/01/16
 * Time: 15:45
 */

require '../vendor/autoload.php';
require '../config/Connec.php';

use racoin\backend\controller\indexController;
use racoin\backend\controller\annonceController;
Connec::Connection('config.ini');

// Connection à la base avec Eloquent
$app = new Slim\Slim(array(
    'view' => new Slim\Views\Twig(),
    'templates.path' => '../src/racoin/backend/templates'
));
$app->contentType('text/html; charset=utf-8');
$app->add(new \Slim\Middleware\SessionCookie(array(
    'expires' => '20 minutes',
    'path' => '/',
    'domain' => null,
    'secure' => false,
    'httponly' => false,
    'name' => 'slim_session',
    'secret' => 'CHANGE_ME',
    'cipher' => MCRYPT_RIJNDAEL_256,
    'cipher_mode' => MCRYPT_MODE_CBC
)));

$app->rootUri = $app->request->getRootUri();

$view = $app->view();
$view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
);

$app->get('/', function() use ($app) {
    indexController::index($app, 'index');
})->name('index');

$app->get('/:action(/:id)', function($action, $id = null) use ($app) {
 indexController::index($app, $action, $id);
})->name('indexAction');
//
 $app->post('/:action(/:id)', function($action, $id = null) use ($app) {
     indexController::index($app, $action, $id);
 })->name('indexActionPost');
//
//$app->get('/admin', function() use ($app) {
//    indexController::admin($app);
//})->name('admin');
//
//$app->get('/deconnexion', function($id) use ($app) {
//    indexController::deconnexion($app);
//})->name('deconnexion');

$app->run();