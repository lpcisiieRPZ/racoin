<?php

require '../vendor/autoload.php';
require '../config/Connec.php';
require '../src/racoin/common/apiKeyFunction.php';

use racoin\backend\controller\annonceController;
use racoin\backend\controller\categorieController;
use racoin\backend\controller\departementsController;
use racoin\api\controller\annonceurController;



// Connection à la base avec Eloquent
Connec::Connection('config.ini');
$app = new Slim\Slim;

$app->get('/', 'check_api_key', function() use ($app) {
    $app->contentType('text/html; charset=utf-8');
	echo "ça marche";
});

// access to an annonce by ID
$app->get('/annonces/:id', 'check_api_key', function($id) use ($app) {
	annonceController::annonceId($app, $id);
})->name('annoncesId');

// access to the related category of the annonce
$app->get('/annonces/:id/categorie', 'check_api_key', function($id) use ($app) {
	categorieController::categorieId($app, $id);
})->name('categAnnonce');

// access to the related department of the annonce
$app->get('/annonces/:id/departement', 'check_api_key', function($id) use ($app) {
	departementsController::departementsId($app, $id);
})->name('depAnnonce');

// access to every annonces
$app->get('/annonces', 'check_api_key', function() use ($app) {
	annonceController::annonces($app);
})->name('annonces');

// access to an categorie by ID
$app->get('/categories/:id', 'check_api_key', function($id) use ($app) {
	categorieController::categorieId($app, $id);
})->name('categoriesId');

// access to every categories
$app->get('/categories', 'check_api_key', function() use ($app) {
	categorieController::categories($app);
})->name('categories');

// access to the related categorie of the annonce
$app->get('/annonces/:id/categorie', 'check_api_key', function($id) use ($app) {
	annonceController::catAnnonces($app, $id);
})->name('annoncesCat');

// access to the related annonces of the categorie
$app->get('/categories/:id/annonces', 'check_api_key', function($id) use ($app) {
	annonceController::annonces($app, $id);
})->name('categoriesAnnonce');

$app->post('/categories/:id/annonces', 'check_api_key',function($id) use ($app) {
	annonceController::addAnnonce($app);
})->name('categoriesAddAnnonce');

// access to an departement by ID
$app->get('/departements/:id', 'check_api_key', function($id) use ($app) {
	departementsController::departementsId($app, $id);
})->name('departementsId');

// access to the related annonceur of the annonce
$app->get('/annonces/:id/annonceur', 'check_api_key', function($id) use ($app){
	annonceurController::viewAnnonceur($app, $id);
});

// access to the related annonce of the annonceur
$app->get('/annonceur/:mail/annonces', 'check_api_key', function($mail) use ($app){
	annonceurController::viewAnnnonceAnnonceur($app, $mail);
});

$app->put('/annonces/:id', 'check_api_key', function($id) use ($app){
	annonceurController::addCoord($app, $id);
});

$app->post('/annonces/', 'check_api_key', function() use ($app){
	annonceController::addAnnonce($app);
})->name('annonceAjout');

$app->put('/annonces/:id/status', 'check_api_key', function($id) use ($app) {
	annonceController::annonceStatus($app, $id);
});

$app->delete('/annonces/:id', 'check_api_key', function($id) use ($app) {
	annonceController::deleteAnnonce($app, $id);
});

$app->run();